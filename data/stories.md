## interactive_story_0
    - utter_greet
* goodbye
    - utter_goodbye

## interactive_story_1
* greet
* restaurant_search
    - utter_ask_location
* location{"location": "jaipur"}
    - slot{"location": "jaipur"}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "south indian"}
    - slot{"cuisine": "south indian"}
    - utter_ask_price
* price{"price_comp": "above", "price2": "500"}
    - slot{"price2": "500"}
    - slot{"price_comp": "above"}
    - action_search_restaurants
    - slot{"restaurants": [{"name": "Dasaprakash", "address": "5, Kamal Mansion, Opposite Tanishq Jewellery, MI Road, Jaipur", "rating": "4.0", "cost": 1000}, {"name": "Sankalp", "address": "32 & 33, 1st Floor, Vidhyut Nagar A, Near Subh Hospital, Prince Road, Vaishali Nagar, Jaipur", "rating": "3.7", "cost": 650}]}
    - utter_ask_feedback
* affirm
    - utter_ask_should_email
* affirm
    - utter_ask_email_id
* send_email{"email_id": "shriyansh.agnihotri.sa@gmail.com"}
    - slot{"email_id": "shriyansh.agnihotri.sa@gmail.com"}
    - action_send_email
    - utter_goodbye
    - action_restart

## interactive_story_2
* greet
* restaurant_search{"cuisine": "south indian", "location": "jaipur", "price_comp": "above", "price2": "500"}
    - slot{"cuisine": "south indian"}
    - slot{"location": "jaipur"}
    - slot{"price2": "500"}
    - slot{"price_comp": "above"}
    - action_search_restaurants
    - slot{"restaurants": [{"name": "Dasaprakash", "address": "5, Kamal Mansion, Opposite Tanishq Jewellery, MI Road, Jaipur", "rating": "4.0", "cost": 1000}, {"name": "Sankalp", "address": "32 & 33, 1st Floor, Vidhyut Nagar A, Near Subh Hospital, Prince Road, Vaishali Nagar, Jaipur", "rating": "3.7", "cost": 650}]}
    - utter_ask_feedback
* affirm
    - utter_ask_should_email
* affirm
    - utter_ask_email_id
* send_email{"email_id": "shriyansh.agnihotri.sa@gmail.com"}
    - slot{"email_id": "shriyansh.agnihotri.sa@gmail.com"}
    - action_send_email
    - utter_goodbye
    - action_restart

## interactive_story_3
* restaurant_search{"cuisine": "south indian", "location": "jaipur", "price_comp": "above", "price2": "500"}
    - slot{"cuisine": "south indian"}
    - slot{"location": "jaipur"}
    - slot{"price2": "500"}
    - slot{"price_comp": "above"}
    - action_search_restaurants
    - slot{"restaurants": [{"name": "Dasaprakash", "address": "5, Kamal Mansion, Opposite Tanishq Jewellery, MI Road, Jaipur", "rating": "4.0", "cost": 1000}, {"name": "Sankalp", "address": "32 & 33, 1st Floor, Vidhyut Nagar A, Near Subh Hospital, Prince Road, Vaishali Nagar, Jaipur", "rating": "3.7", "cost": 650}]}
    - utter_ask_feedback
* affirm
    - utter_ask_should_email
* affirm
    - utter_ask_email_id
* send_email{"email_id": "shriyansh.agnihotri.sa@gmail.com"}
    - slot{"email_id": "shriyansh.agnihotri.sa@gmail.com"}
    - action_send_email
    - utter_goodbye
    - action_restart

## interactive_story_4
* restaurant_search{"cuisine": "south indian", "location": "jaipur", "price_comp": "above", "price2": "500"}
    - slot{"cuisine": "south indian"}
    - slot{"location": "jaipur"}
    - slot{"price2": "500"}
    - slot{"price_comp": "above"}
    - action_search_restaurants
    - slot{"restaurants": [{"name": "Dasaprakash", "address": "5, Kamal Mansion, Opposite Tanishq Jewellery, MI Road, Jaipur", "rating": "4.0", "cost": 1000}, {"name": "Sankalp", "address": "32 & 33, 1st Floor, Vidhyut Nagar A, Near Subh Hospital, Prince Road, Vaishali Nagar, Jaipur", "rating": "3.7", "cost": 650}]}
    - utter_ask_feedback
* affirm
    - utter_ask_should_email
* affirm
    - utter_ask_email_id
* send_email{"email_id": "shriyansh.agnihotri.sa@gmail.com"}
    - slot{"email_id": "shriyansh.agnihotri.sa@gmail.com"}
    - action_send_email
    - utter_goodbye
    - action_restart

## interactive_story_4
* restaurant_search{"cuisine": "south indian", "location": "jaipur", "price_range": "mid"}
    - slot{"cuisine": "south indian"}
    - slot{"location": "jaipur"}
    - slot{"price2": "500"}
    - slot{"price_comp": "above"}
    - action_search_restaurants
    - slot{"restaurants": [{"name": "Dasaprakash", "address": "5, Kamal Mansion, Opposite Tanishq Jewellery, MI Road, Jaipur", "rating": "4.0", "cost": 1000}, {"name": "Sankalp", "address": "32 & 33, 1st Floor, Vidhyut Nagar A, Near Subh Hospital, Prince Road, Vaishali Nagar, Jaipur", "rating": "3.7", "cost": 650}]}
    - utter_ask_feedback
* affirm
    - utter_ask_should_email
* affirm
    - utter_ask_email_id
* send_email{"email_id": "shriyansh.agnihotri.sa@gmail.com"}
    - slot{"email_id": "shriyansh.agnihotri.sa@gmail.com"}
    - action_send_email
    - utter_goodbye
    - action_restart

## interactive_story_5
* restaurant_search{"location": "bangalore", "cuisine": "italion"}
    - slot{"cuisine": "italion"}
    - slot{"location": "bangalore"}
    - utter_ask_price
* restaurant_search{"price_range": "low"}
    - slot{"price_range": "low"}
    - action_search_restaurants
    - slot{"restaurants": []}
    - utter_ask_feedback
* affirm
    - utter_ask_should_email
* deny
    - utter_goodbye
    - action_restart

## interactive_story_1
* restaurant_search
    - utter_ask_location
* location{"location": "Bombay"}
    - slot{"location": "Bombay"}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "mexican"}
    - slot{"cuisine": "mexican"}
    - utter_ask_price
* price{"price_range": "high"}
    - slot{"price_range": "high"}
    - action_search_restaurants
    - slot{"restaurants": []}
    - utter_ask_feedback
* deny
    - utter_goodbye
    - action_restart
    
## interactive_story_1
* invalid
    - utter_default
* invalid{"location": "bangalore"}
    - slot{"location": "bangalore"}
* goodbye
    - utter_goodbye
    - action_restart

## interactive_story_1
* greet
    - utter_greet
* restaurant_search{"location": "bangalore"}
    - slot{"location": "bangalore"}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "italian"}
    - slot{"cuisine": "italian"}
    - utter_ask_price
* price{"price_range": "avg"}
    - slot{"price_range": "avg"}
    - action_search_restaurants
    - slot{"restaurants": [{"name": "Toit", "address": "298, 100 Feet Road, Namma Metro Pillar 62, Indiranagar, Bangalore", "rating": "4.5", "cost": 2000}, {"name": "Pasta Street", "address": "751, 80 Feet Road, Opposite SBI Bank, Koramangala 4th Block, Bangalore", "rating": "4.7", "cost": 1500}, {"name": "Simon Says Brew Works", "address": "105, 3rd Floor, 1st A Cross Road, Jyothi Nivas College Road, Koramangala 5th Block, Bangalore", "rating": "4.5", "cost": 1200}, {"name": "Pasta Street", "address": "2022, 100 Feet Road,HAL 2nd Stage, Indiranagar, Bangalore", "rating": "4.6", "cost": 1500}, {"name": "Smoke House Deli", "address": "1209, 100 Feet Road, Opposite Apollo Clinic, Indiranagar, Bangalore", "rating": "4.5", "cost": 1600}, {"name": "Smoke - The Sizzler House", "address": "71, 80 Feet Road, RMV 2nd Stage, New BEL Road, Bangalore", "rating": "4.3", "cost": 800}, {"name": "Smoke House Deli", "address": "52/ 53, Ground Floor, Lavelle Road, Bangalore", "rating": "4.6", "cost": 1600}, {"name": "Red Tomato", "address": "11, 42nd A Cross Road, 5th Block, Jayanagar, Bangalore", "rating": "4.1", "cost": 1400}, {"name": "Pizza Bakery - Brew & Eatery", "address": "2985, 12th Main, HAL 2nd Stage, Indiranagar, Bangalore", "rating": "4.5", "cost": 1200}, {"name": "Toscano - Mojito Bar", "address": "3rd Floor, VR Mall, Whitefield Main Road, Mahadevpura, Whitefield, Bangalore", "rating": "4.6", "cost": 2400}]}
    - utter_ask_feedback
* deny
    - utter_goodbye
    - action_restart

## interactive_story_2
* invalid
    - utter_default
* restaurant_search
    - utter_ask_location
* location{"location": "kolkata"}
    - slot{"location": "kolkata"}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "north indian"}
    - slot{"cuisine": "north indian"}
    - utter_ask_price
* price{"price_range": "low"}
    - slot{"price_range": "low"}
    - action_search_restaurants
    - slot{"restaurants": [{"name": "Pappu Biryani House", "address": "80, Bidhan Sarani, Hatibagan, Kolkata", "rating": "3.6", "cost": 200}]}
    - utter_ask_feedback
* affirm
    - utter_ask_should_email
* affirm
    - utter_ask_email_id
* send_email{"email_id": "shriyansh.agnihotri.sa@gmail.com"}
    - slot{"email_id": "shriyansh.agnihotri.sa@gmail.com"}
    - action_send_email
    - utter_goodbye
    - action_restart

## interactive_story_1
* restaurant_search
    - utter_ask_location
* restaurant_search{"location": "mysore"}
    - slot{"location": "mysore"}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "italian"}
    - slot{"cuisine": "italian"}
    - utter_ask_price
* restaurant_search{"price_range": "high"}
    - slot{"price_range": "high"}
    - action_search_restaurants
    - slot{"restaurants": [{"name": "The Old House", "address": "451, Jhansi Rani Lakshmi Bai Road, Chamrajpura, Mysore", "rating": "4.6", "cost": 800}, {"name": "Cafe Cornucopia", "address": "22nd Cross, 14th Main, 3rd Stage, Block C, Vijay Nagar, Mysore", "rating": "4.1", "cost": 800}, {"name": "The Barge Restaurant", "address": "Plot 440A, Hebbal Industrial Area, Near Infosys Campus, Vijay Nagar, Mysore", "rating": "4.3", "cost": 1500}, {"name": "Jalpaan Dining Saga", "address": "366, 2nd & 3rd Floor, Near Ramaswamy Circle, Chamaraja Mohalla, Chamrajpura, Mysore", "rating": "4.3", "cost": 900}, {"name": "Sepoy's Phirangi", "address": "Opp JSS Hospital,  MG Road, Chamrajpura, Mysore", "rating": "3.9", "cost": 1000}, {"name": "Olive Garden", "address": "The Wind Flower Resorts & Spa, Jayachamaraja Golf Club Maharanapratap Road, Nazarbad, Chamundipuram, Ittige Gudu, Mysore", "rating": "4.1", "cost": 1250}, {"name": "Rhythm N Brews", "address": "112, 8th Main Road, 2nd Block, Jayalakshmipuram, Mysore", "rating": "4.3", "cost": 750}]}
    - utter_ask_feedback
* affirm
    - utter_ask_should_email
* deny
    - utter_goodbye
    - action_restart
