## intent:affirm
- yeah
- thanks
- ok

## intent:deny
- sorry
- danm

## intent:invalid
- what is the capital of [bangalore](location)
- give me a dog

## intent:goodbye
- have a good one
- good bye

## intent:greet
- dear sir
- yo
- hi

## intent:location
- [Indore](location)
- [Bhavnagar](location)
- [Coimbatore Nagpur](location)
- [Mumbai](location)
- [Surat](location)
- [Raipur](location)
- [Ahmedabad](location)
- [Amravati](location)
- [Asansol](location)
- [Gorakhpur](location)
- [Chennai](location)
- [Bhiwandi](location)
- [Goa](location)
- [Guwahati](location)
- [Bikaner](location)
- [Kakinada](location)
- [Tiruppur](location)
- [Bhilai](location)
- [Dhanbad](location)
- [Bokaro Steel City](location)
- [Hyderabad](location)
- [Bareilly](location)

## intent:price
- anything [expensive](price_range:high)
- [more](price_comp) then [700](price2)
- [mid](price_range) range for 2
- around [500](price2) [above](price_comp)

## intent:send_email
- [shriy@gmail.com](email_id)

## intent:restaurant_search
- fetch me food in [jaipur](location) in [mid](price_range) range
- show me restaurants
- Oh, sorry, in [Italy](location)
- [delhi](location)
- I am looking for [mexican indian fusion](cuisine)
- [mumbai](location)
- in [delhi](location)
- get me food in [chennai](location)
- get me restaurant for [american](cuisine) food
- [Lithuania](location)
- [North Indian](cuisine)
- i am going on an [expensive](price_range:high) date something in 5 star hotel
- [Italian](cuisine:italian)

## synonym:4
- four

## synonym:Delhi
- del
- New Delhi
- Dilli

## synonym:Mumbai
- mumbai
- Bom
- Mum
- bom
- bombay
- Bombay

## synonym:american
- American

## synonym:bangalore
- Bengaluru
- blr
- banglr

## synonym:chinese
- chines
- Chinese
- Chines

## synonym:false
- False

## synonym:high
- expensive
- rich

## synonym:italian
- Italian

## synonym:low
- cheap
- best value
- normal

## synonym:mexican
- Mexican

## synonym:mid
- moderate
- avg

## synonym:no
- No
- NO

## synonym:north indian
- North Indian

## synonym:south indian
- South Indian

## synonym:true
- True

## synonym:vegetarian
- veggie
- vegg

## synonym:yes
- Yes
- YES

## regex:email_id
- ([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})

## regex:greet
- hey[^\s]*

## regex:pincode
- [0-9]{6}
