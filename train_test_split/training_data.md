## intent:affirm
- that's right
- great
- awesome
- yes
- sounds really good
- True
- right, thank you
- great choice
- correct
- indeed
- yep

## intent:deny
- false
- No
- no thanks
- not required

## intent:invalid
- i am very hungup
- i am awesome
- find me restroom
- I am not hungry
- i am very hung
- once upon a time i was stupid

## intent:goodbye
- bye bye
- bye
- farewell
- goodbye
- end
- Bye bye
- thanks a lot
- stop

## intent:greet
- hey there
- whatssuppp
- Hey
- hey
- good morning
- Hello
- good evening
- hello
- howdy

## intent:location
- [Lucknow](location)
- [Kollam](location)
- [Erode](location)
- [Gurgaon](location)
- [Hubli–Dharwad](location)
- [Vijayawada](location)
- [Durgapur](location)
- [Visakhapatnam](location)
- [Madurai](location)
- [Siliguri](location)
- [Ujjain](location)
- [Kannur](location)
- [Malappuram](location)
- [Bhubaneswar](location)
- [Jodhpur](location)
- [Jabalpur](location)
- [Kochi](location)
- [Mangalore](location)
- [Bijapur](location)
- [Mysore](location)
- [Belgaum](location)
- [Salem](location)
- [Jaipur](location)
- [Nellore](location)
- [jaipur](location)
- [Jammu](location)
- [Shimla](location)
- [Vadodara](location)
- [Rajahmundry](location)
- [Jamnagar](location)
- [Ajmer](location)
- [Firozabad](location)
- [Nashik](location)
- [Warangal](location)
- [Tiruchirappalli](location)
- [Solapur](location)
- [Gwalior](location)
- [Pondicherry](location)
- from [mumbai](location:Mumbai)
- [Srinagar](location)
- [Meerut](location)
- i am in [jaipur](location)
- [Noida](location)
- [Bhopal](location)
- [Thrissur](location)
- I am in [mumbai](location:Mumbai)
- [Chandigarh](location)
- [Ghaziabad](location)
- [Thiruvananthapuram](location)
- [Bangalore](location)
- [Purulia Prayagraj](location)
- [Pune](location)
- [Kolkata](location)
- [Aligarh](location)
- [Jalandhar](location)
- [Moradabad](location)
- [Amritsar](location)
- [Dehradun](location)
- [Bilaspur](location)
- [Kozhikode](location)
- [Kanpur](location)
- [Nanded](location)
- [Ludhiana](location)
- [Patna](location)
- [Mathura](location)
- [Vellore](location)
- [del](location:Delhi)
- [Kurnool](location)
- [Gulbarga](location)
- [Varanasi](location)
- from [kolkata](location)
- [Cuttack](location)
- [Aurangabad](location)
- [Jhansi](location)
- [Sangli](location)
- [Faridabad](location)
- [Vasai-Virar City](location)
- [Agra](location)
- [Ranchi](location)
- [Hamirpur[disambiguation needed]](location)
- [Kolhapur](location)
- [Guntur](location)
- [bhopal](location)
- [Rourkela](location)
- [Jamshedpur](location)
- [Rajkot](location)
- [Delhi](location)
- [mysore](location)

## intent:price
- [expensive](price_range:high)
- [between](price_comp) [200](price1) and [600](price2)
- [below](price_comp) [300](price1)
- [under](price_comp) [300](price1)
- [cheap](price_range:low)
- [above](price_comp) [300](price2)
- [best value](price_range:low)
- something [above](price_comp) [500](price2)
- [mid](price_range)
- in [avg](price_range) range
- [moderate](price_range:mid)
- [above](price_comp) [650](price2)
- anything [above](price_comp) [600](price2)
- [less](price_comp) than [300](price1)
- [less](price_comp) than [400](price1)

## intent:send_email
- [shriyansh.agnihotri.sa@gmail.com](email_id)
- email id is [shr.ggggiy@gmdddail.m](email_id)
- my email id is [shri.dfdfy@gmaffgil.om](email_id)

## intent:restaurant_search
- get me a restaurant near me
- [American](location:american)
- [cheap](price_range:low) food
- get me food
- show me [chinese](cuisine) restaurants
- show me [chines](cuisine:chinese) restaurants in the [New Delhi](location:Delhi)
- I am looking a restaurant in [294328](location)
- i am situated in [mysore](location)
- show me a [mexican](cuisine) place in the [centre](location)
- in [Gurgaon](location)
- give me restarunt in [bangalore](location) for [italion](cuisine) cuisine
- I am looking for some restaurants in [Delhi](location).
- i am very hungry
- [chinese](cuisine)
- want to know some resturants near me
- i'm looking for a place to eat
- [Chinese](cuisine:chinese)
- anywhere in the [west](location)
- i am hungry
- I want to grab lunch
- [Italian](cuisine)
- [Mexican](cuisine:mexican)
- I am looking for some restaurants in [Bangalore](location)
- [South Indian](cuisine)
- I am looking for some restaurants in [Mumbai](location)
- i want restaurant [under](price_comp) [900](price2) for 2
- give me a restaurant in [jaipur](location)
- please show me a few [italian](cuisine) restaurants in [bangalore](location)
- find me a [chines](cuisine:chinese) restaurant
- please help me to find restaurants in [pune](location)
- give me an [american](cuisine) restaurant in [jaipur](location)
- restaurant [over](price_comp) budget of [400](price2)
- restaurant [under](price_comp) budget of [400](price1)
- Please find me a restaurantin [bangalore](location)
- give me food
- I am looking for [asian fusion](cuisine) food
- i am looking for an [indian](cuisine) spot called olaolaolaolaolaola
- get me a resturant near [pune](location) [over](price_comp) [700](price2) for 2
- restaurant [within](price_comp) budget of [400](price1) and [500](price2)
- can you book a table in [rome](location) in a [moderate](price_range:mid) price range with [british](cuisine) food for [four](people:4) people
- help to find restarunnt near [hyderabad](location) with [north indian](cuisine) food of [high](price_range) budget
- find me [south indian](cuisine) food in [bangalore](location) [under](price_comp) budget of [400](price1)
- search for restaurants
- [central](location) [indian](cuisine) restaurant
- [cheap](price_range:low) food will work
- [North Indian](cuisine:north indian)
- please find me [chinese](cuisine) restaurant in [delhi](location)
- get me places to have meal in [bangalore](location)
- can you find me a [chinese](cuisine) restaurant
- please find me a restaurant in [ahmedabad](location)
- [South Indian](cuisine:south indian)
- I am searching for a dinner spot

## synonym:4
- four

## synonym:Delhi
- del
- New Delhi
- Dilli

## synonym:Mumbai
- mumbai
- Bom
- Mum
- bom
- bombay
- Bombay

## synonym:american
- American

## synonym:bangalore
- Bengaluru
- blr
- banglr

## synonym:chinese
- chines
- Chinese
- Chines

## synonym:false
- False

## synonym:high
- expensive
- rich

## synonym:italian
- Italian

## synonym:low
- cheap
- best value
- normal

## synonym:mexican
- Mexican

## synonym:mid
- moderate
- avg

## synonym:no
- No
- NO

## synonym:north indian
- North Indian

## synonym:south indian
- South Indian

## synonym:true
- True

## synonym:vegetarian
- veggie
- vegg

## synonym:yes
- Yes
- YES

## regex:email_id
- ([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})

## regex:greet
- hey[^\s]*

## regex:pincode
- [0-9]{6}
